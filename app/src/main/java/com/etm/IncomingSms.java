package com.etm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * Created by appcraftz on 7/10/17.
 */

public class IncomingSms extends BroadcastReceiver{
    final SmsManager sms = SmsManager.getDefault();
    @Override
    public void onReceive(Context context, Intent intent) {

        final Bundle bundle = intent.getExtras();

        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();//.split(":")[2]

                    if(message.contains("Regards ETM Team")){

                    String[] parts = message.split("\\s+");
                            //message = message.substring(0, message.length()-1);
                        for (int j=0;j<parts.length;j++){
                            Log.e("SubString: ",parts[j]);
                        }
                    Log.e("SmsReceiver", "senderNum: " + senderNum + "; message: " + message);

                    Intent myIntent = new Intent("otp");
                    myIntent.putExtra("message",parts[3]);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(myIntent);
                    }else {
                        Log.e("In IncomingSms","Message from another Sender");
                    }
                    // Show Alert

                } // end for loop
            } // bundle is null

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" +e);

        }
    }
}
