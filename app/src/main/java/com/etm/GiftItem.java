package com.etm;

import java.io.Serializable;
import java.util.StringJoiner;

/**
 * Created by appcraftz on 3/10/17.
 */

public class GiftItem implements Serializable {
    String path;
    String joining;
    String bankBalance;
    String benefit;
    GiftItem(String joining, String bankBalance, String benefit){

        this.joining = joining;
        this.bankBalance = bankBalance;
        this.benefit = benefit;
    }

    public String getPath() {
        return path;
    }

    public String getBankBalance() {
        return bankBalance;
    }

    public String getBenefit() {
        return benefit;
    }

    public String getJoining() {
        return joining;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setBankBalance(String bankBalance) {
        this.bankBalance = bankBalance;
    }

    public void setBenefit(String benefit) {
        this.benefit = benefit;
    }

    public void setJoining(String joining) {
        this.joining = joining;
    }
}

