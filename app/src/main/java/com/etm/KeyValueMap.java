package com.etm;

/**
 * Created by appcraftz on 4/10/17.
 */

class KeyValueMap {

        private String key;
        private String value;

        public KeyValueMap(String key,String value)
        {
            this.key = key;
            this.value = value;
        }
        public String getKey()
        {
            return key;
        }
        public String getValue()
        {
            return value;
        }


}
