package com.etm;

import java.io.Serializable;

/**
 * Created by SSC on 17-Oct-17.
 */

public class MaxItem implements Serializable {
    String referCode,noOfJoining;

    public MaxItem(String referCode,String noOfJoining){
        this.referCode = referCode;
        this.noOfJoining = noOfJoining;
    }

    public String getNoOfJoining() {
        return noOfJoining;
    }

    public void setNoOfJoining(String noOfJoining) {
        this.noOfJoining = noOfJoining;
    }

    public String getReferCode() {
        return referCode;
    }

    public void setReferCode(String referCode) {
        this.referCode = referCode;
    }
}