package com.etm;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by appcraftz on 13/9/17.
 */

public class SimpleFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context context;


    public SimpleFragmentPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0)
            return new Dashboard();
        else if (position == 1)
            return new Gifts();
        else
            return new About();
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "Account";
            case 1:
                return "Gifts";
            case 2:
                return "About";
            default:
                return null;
        }
    }
}
