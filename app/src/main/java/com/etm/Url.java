package com.etm;

/**BR7383777
 * Created by appcraftz on 4/10/17.
 */

public class Url {

    public static final String serverUrl = "http://13.228.218.242/~appcrf/development/ETM/";
    public static final String register = serverUrl + "register.php";

    public static final String login = serverUrl +  "login.php";

    public static final String verifyOtp = serverUrl + "verify_otp.php";
    public static final String verifyReferralCode = serverUrl +  "key_verification.php";
    public static final String getMaxItems = serverUrl +  "max_five.php";
    public static final String getMyJoining = serverUrl + "user_details.php";
    public static final String getReferredByDetails = serverUrl + "getReferredBy.php";
}
