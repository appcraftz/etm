package com.etm;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class Login extends AppCompatActivity {

    Button login;
    TextView signUp;
    EditText uName, password;
    String user,pwd;
    ProgressDialog progressdialog;
    NetworkStatus networkStatus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    @Override
    protected void onStart() {
        super.onStart();
        networkStatus = NetworkStatus.getInstance(Login.this);
        final ServerAgent agent = new ServerAgent(Login.this);
        login = (Button)findViewById(R.id.etmLogin);
        signUp = (TextView)findViewById(R.id.signUp);
        uName = (EditText)findViewById(R.id.etmUserName);
        password = (EditText)findViewById(R.id.etmPassword);
        progressdialog = new ProgressDialog(Login.this);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                    if (uName.getText().toString().equals("") || password.getText().toString().equals("")) {
                        Toast.makeText(Login.this, "Please Fill the Login Credentials", Toast.LENGTH_SHORT).show();
                    }/*else if(uName.getText().toString().equals("9766789876") || password.getText().toString().equals("qaz")){
                    if (SPHelper.getSP(Login.this,"referred").equals("true")){
                        startActivity(new Intent(Login.this, MainActivity.class));
                        finish();
                    }else {
                        startActivity(new Intent(Login.this, ReferralCode.class));
                        finish();
                    }
                }*/ else {
                        if (networkStatus.isOnline()) {
                            new AsyncTask<Void, Void, ArrayList<KeyValueMap>>() {
                                @Override
                                protected void onPreExecute() {
                                    super.onPreExecute();
                                    progressdialog.setMessage("Please Wait Validating Credentials...");
                                    progressdialog.setCancelable(false);
                                    progressdialog.show();
                                    user = uName.getText().toString();
                                    pwd = password.getText().toString();
                                    SPHelper.setSP(Login.this, "myNumber", uName.getText().toString());

                                }

                                @Override
                                protected ArrayList<KeyValueMap> doInBackground(Void... voids) {
                                    ArrayList<KeyValueMap> resp = agent.login(user, pwd);

                                    return resp;
                                }

                                @Override
                                protected void onPostExecute(ArrayList<KeyValueMap> response) {
                                    super.onPostExecute(response);
                                    progressdialog.dismiss();

                                    if (response == null || response.size()==0){
                                        Toast.makeText(Login.this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                                    }else {

                                        Log.e("REsponse Size",String.valueOf(response.size()));
                                    if (response.get(0).getValue().equals("ENTERED")) {

                                        SPHelper.setSP(Login.this,"isLoggedIn","true");

                                            if (response.get(1) != null) {


                                                //Own Referre Code
                                                SPHelper.setSP(Login.this, "myReferCode", response.get(1).getValue());

                                                if (!response.get(2).getValue().equals("NULL")) {
                                                    //Referred by Employee Id
                                                    SPHelper.setSP(Login.this, "referred_by", response.get(2).getValue());
                                                    SPHelper.setSP(Login.this, "referred", "true");
                                                } else {
                                                    SPHelper.setSP(Login.this, "referred_by", "NULL");
                                                    SPHelper.setSP(Login.this, "referred", "false");
                                                }


                                                Log.e("My ReferCode", response.get(1).getValue());
                                                Log.e("Referred By employee", response.get(2).getValue());

                                                if (SPHelper.getSP(Login.this, "referred").equals("true")) {
                                                    //SPHelper.setSP(Login.this,"isLoggedIn","true");
                                                    startActivity(new Intent(Login.this, MainActivity.class));
                                                    finish();
                                                } else {
                                                    startActivity(new Intent(Login.this, ReferralCode.class));
                                                    finish();
                                                }
                                            }

                                        } else if (response.get(0).getValue().equals("THE_END")) {
                                            Toast.makeText(Login.this, "Please Enter Valid Credentials", Toast.LENGTH_SHORT).show();
                                        }
                                    }/*else {
                                        Toast.makeText(Login.this, "Unable to Login", Toast.LENGTH_SHORT).show();
                                    }*/
                                }
                            }.execute(null, null, null);
                        }else {
                            Toast.makeText(Login.this, "Check Your Internet Connection...!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

        });
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Login.this,SignUp.class));
                finish();
            }
        });
    }
}
