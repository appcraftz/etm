package com.etm;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by SSC on 17-Oct-17.
 */

public class MaxItemAdapter extends RecyclerView.Adapter<MaxItemAdapter.viewHolder> {

    ArrayList<MaxItem> maxItems;
    Context context;

    MaxItemAdapter(Context context,ArrayList<MaxItem> maxItems){
        this.context = context;
        this.maxItems = maxItems;
    }
    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v1;
        v1 = (LayoutInflater.from(context)).inflate(R.layout.maxitem,null);
        return new viewHolder(v1);
    }

    @Override
    public void onBindViewHolder(viewHolder holder, int position) {

        holder.referralCode.setText(maxItems.get(position).getReferCode());
        holder.noOfJoining.setText("Total Joining : "+maxItems.get(position).getNoOfJoining());
    }

    @Override
    public int getItemCount() {
        if(maxItems!=null)
            return maxItems.size();
        else
            return 0;
    }

    public class viewHolder extends RecyclerView.ViewHolder{

        TextView referralCode,noOfJoining;
        public viewHolder(View itemView) {
            super(itemView);

            referralCode = (TextView)itemView.findViewById(R.id.maxReferCode);
            noOfJoining = (TextView)itemView.findViewById(R.id.maxNoOfJoining);
        }
    }
}
