package com.etm;

/**
 * Created by appcraftz on 5/10/17.
 */

public class SignUpItem {
    String sName,sAddress, sMobileNo, bAccountNo, bAddress, bIFSC, sAadhar, sPANcard,sPassword,sGender,aDistAgency,aTalAgency;

    SignUpItem(String sName,String sAddress,String sMobileNo, String bAccountNo, String bAddress, String bIFSC, String sAadhar, String sPANcard,String sPassword,String sGender,String aDistAgency,String aTalAgency){
        this.sName = sName;
        this.sAddress = sAddress;
        this.sMobileNo = sMobileNo;
        this.bAccountNo = bAccountNo;
        this.bAddress = bAddress;
        this.bIFSC = bIFSC;
        this.sAadhar = sAadhar;
        this.sPANcard = sPANcard;
        this.sPassword = sPassword;
        this.sGender = sGender;
        this.aDistAgency = aDistAgency;
        this.aTalAgency =aTalAgency;
    }


    public String getbAccountNo() {
        return bAccountNo;
    }

    public String getaDistAgency() {
        return aDistAgency;
    }

    public String getaTalAgency() {
        return aTalAgency;
    }

    public String getbAddress() {
        return bAddress;
    }

    public String getbIFSC() {
        return bIFSC;
    }

    public String getsAadhar() {
        return sAadhar;
    }

    public String getsAddress() {
        return sAddress;
    }

    public String getsGender() {
        return sGender;
    }

    public String getsMobileNo() {
        return sMobileNo;
    }

    public String getsName() {
        return sName;
    }

    public String getsPANcard() {
        return sPANcard;
    }

    public String getsPassword() {
        return sPassword;
    }


    public void setaDistAgency(String aDistAgency) {
        this.aDistAgency = aDistAgency;
    }

    public void setaTalAgency(String aTalAgency) {
        this.aTalAgency = aTalAgency;
    }

    public void setbAccountNo(String bAccountNo) {
        this.bAccountNo = bAccountNo;
    }

    public void setbAddress(String bAddress) {
        this.bAddress = bAddress;
    }

    public void setbIFSC(String bIFSC) {
        this.bIFSC = bIFSC;
    }

    public void setsAadhar(String sAadhar) {
        this.sAadhar = sAadhar;
    }

    public void setsAddress(String sAddress) {
        this.sAddress = sAddress;
    }

    public void setsGender(String sGender) {
        this.sGender = sGender;
    }

    public void setsMobileNo(String sMobileNo) {
        this.sMobileNo = sMobileNo;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public void setsPANcard(String sPANcard) {
        this.sPANcard = sPANcard;
    }

    public void setsPassword(String sPassword) {
        this.sPassword = sPassword;
    }
}
