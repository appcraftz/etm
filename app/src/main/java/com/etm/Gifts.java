package com.etm;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Gifts#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Gifts extends Fragment {
    RecyclerView giftView;
    ArrayList<GiftItem> giftItems;
    GiftAdapter adapter;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public Gifts() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Gifts.
     */
    // TODO: Rename and change types and number of parameters
    public static Gifts newInstance(String param1, String param2) {
        Gifts fragment = new Gifts();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_gifts, container, false);
    }


    @Override
    public void onStart() {
        super.onStart();

        giftView = (RecyclerView)getView().findViewById(R.id.giftView);

        giftItems = new ArrayList<GiftItem>();

        setupList();
        adapter = new GiftAdapter(getActivity(),giftItems);

        GridLayoutManager manager = new GridLayoutManager(getActivity(),2);
        //LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
//        layoutManager.setStackFromEnd(true);

        giftView.setLayoutManager(manager);
        giftView.setAdapter(adapter);
    }

    private void setupList() {
        giftItems.add(new GiftItem("Joinings : 10","Account Credit : 500","Gift : Alarm Lock For Your Home Security"));
        giftItems.add(new GiftItem("Joinings : 20","Account Credit : 1000","Gift : Solar Power bank for Your Mobile Charging"));
        giftItems.add(new GiftItem("Joinings : 50","Account Credit : 2500","Gift : Roti Maker+Aata Maker+Hot Pot Combo Pack for Your Kitchen"));
        giftItems.add(new GiftItem("Joinings : 60","Account Credit : 3000","Gift : Induction Cooking"));
        giftItems.add(new GiftItem("Joinings : 70","Account Credit : 3500","Gift : Air Sofa Cum Bed"));
        giftItems.add(new GiftItem("Joinings : 90","Account Credit : 4500","Gift : Gas Sever Device for Your Gas Cylinder Protection"));
        giftItems.add(new GiftItem("Joinings : 100","Account Credit : 5000","Gift : Juicer Mixer Grinder"));
        giftItems.add(new GiftItem("Joinings : 150","Account Credit : 7500","Gift : 7 Pcs Non-Stick Cookware Set "));
        giftItems.add(new GiftItem("Joinings : 200","Account Credit : 10000","Gift : 50 Pcs Crockery Set"));
        giftItems.add(new GiftItem("Joinings : 250","Account Credit : 12500","Gift : 4 B Glass Gas StoveWith Gas Lighter"));
        giftItems.add(new GiftItem("Joinings : 300","Account Credit : 15000","Gift : Automatic Washing Machine"));
        giftItems.add(new GiftItem("Joinings : 400","Account Credit : 20000","Gift : Invertors + Battery For Home"));
        giftItems.add(new GiftItem("Joinings : 500","Account Credit : 25000","Gift : Samsung 190 Ltr Refrigerator"));
        giftItems.add(new GiftItem("Joinings : 600","Account Credit : 30000","Gift : Sony Bravia 32 LED TV"));
        giftItems.add(new GiftItem("Joinings : 700","Account Credit : 35000","Gift : Screen Touch Laptop"));
        giftItems.add(new GiftItem("Joinings : 900","Account Credit : 45000","Gift : IFD Dishwasher (Croma VX)"));
        giftItems.add(new GiftItem("Joinings : 1000","Account Credit : 50000","Gift : Honda CLIQ Scooty"));
        giftItems.add(new GiftItem("Joinings : 1500","Account Credit : 75000","Gift : Honda Motor Bike"));
        giftItems.add(new GiftItem("Joinings : 2000","Account Credit : 100000","Gift : Sony Full HD Android 60 Inch TV  "));
        giftItems.add(new GiftItem("Joinings : 2500","Account Credit : 125000","Gift : 6 Days Thailand Tour Package for 2 Persons by Kesari Tours\n"));
        giftItems.add(new GiftItem("Joinings : 3000","Account Credit : 150000","Gift : Yamaha Fazer 250 Bike"));
        giftItems.add(new GiftItem("Joinings : 5000","Account Credit : 250000","Gift : Enfield 346 cc Bullet"));
        giftItems.add(new GiftItem("Joinings : 5500","Account Credit : 275000","Gift : Enfield 500 cc Classic  Bullet"));
        giftItems.add(new GiftItem("Joinings : 6000","Account Credit : 300000","Gift : Maruti Alto car"));
        giftItems.add(new GiftItem("Joinings : 7500","Account Credit : 375000","Gift : Datson Redi Go Car"));
        giftItems.add(new GiftItem("Joinings : 9000","Account Credit : 450000","Gift : Renault Kwid Car"));
        giftItems.add(new GiftItem("Joinings : 10000","Account Credit : 500000","Gift : Maruti Celerio Car"));
        giftItems.add(new GiftItem("Joinings : 11000","Account Credit : 550000","Gift : Mahindra KUL 100 Car"));
        giftItems.add(new GiftItem("Joinings : 12000","Account Credit : 600000","Gift : Hyundai i10 Car"));
        giftItems.add(new GiftItem("Joinings : 15000","Account Credit : 750000","Gift : Maruti Suzuki Ertica"));
    }
}
