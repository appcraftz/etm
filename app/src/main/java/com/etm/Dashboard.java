package com.etm;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Dashboard#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Dashboard extends Fragment {
    TextView myReferCode;
    MaxItemAdapter adapter;
    ArrayList<MaxItem> maxItems;
    ServerAgent agent;
    RecyclerView max;
    TextView myJoining,myAmount;

    ArrayList<KeyValueMap> top;
    String myJoin,myAmt;
    String myrefer;
    ArrayList<MaxItem> maximumItems;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public Dashboard() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Dashboard.
     */
    // TODO: Rename and change types and number of parameters
    public static Dashboard newInstance(String param1, String param2) {
        Dashboard fragment = new Dashboard();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }




    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();


        myReferCode = (TextView)getView().findViewById(R.id.myReferCode);
        max = (RecyclerView)getView().findViewById(R.id.maxItemsView);
        myJoining = (TextView)getView().findViewById(R.id.myJoining);
        myAmount = (TextView)getView().findViewById(R.id.myAmount);

        NetworkStatus networkStatus = NetworkStatus.getInstance(getActivity());


        agent = new ServerAgent(getActivity());
        if (SPHelper.getSP(getActivity(),"myReferCode")!=null)
            myrefer =   SPHelper.getSP(getActivity(),"myReferCode");

        if (networkStatus.isOnline()){

            new AsyncTask<Void, Void, ArrayList<KeyValueMap>>() {
                @Override
                protected ArrayList<KeyValueMap> doInBackground(Void... params) {
                    ArrayList<KeyValueMap> resp = agent.myJoining(SPHelper.getSP(getActivity(),"myNumber"));

                    return resp;
                }


                @Override
                protected void onPostExecute(ArrayList<KeyValueMap> keyValueMaps) {
                    super.onPostExecute(keyValueMaps);

                   /* if (keyValueMaps != null || keyValueMaps.size()!=0){
                        if (!keyValueMaps.get(0).getValue().equals("null"))
                            myJoin = keyValueMaps.get(0).getValue();
                        else
                            myJoin = "0";*/
                        if (!keyValueMaps.get(0).getValue().equals("null"))
                            myJoining.setText(keyValueMaps.get(0).getValue());
                        else
                            myJoining.setText("0");
                        if (keyValueMaps!=null || !keyValueMaps.get(1).getValue().equals("null"))
                       myAmount.setText(keyValueMaps.get(1).getValue());
                            //    myAmt = keyValueMaps.get(1).getValue();
                        else
                            myAmount.setText("0");//myAmt= "0";//


                }
            }.execute(null,null,null);


            new AsyncTask<Void, Void, ArrayList<MaxItem>>() {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected ArrayList<MaxItem> doInBackground(Void... params) {
                    maxItems = agent.getMaxUsers();
                    return maxItems;
                }

                @Override
                protected void onPostExecute(ArrayList<MaxItem> maxItems) {
                    super.onPostExecute(maxItems);

                    adapter = new MaxItemAdapter(getActivity(),maxItems);
                    LinearLayoutManager manager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
                    max.setAdapter(adapter);
                    max.setLayoutManager(manager);
                    adapter.notifyDataSetChanged();


                }
            }.execute(null,null,null);
        }
        else {

            myJoining.setText("0");
            myAmount.setText("0");
            Toast.makeText(getActivity(), "Check Your Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

//        myJoining.setText(myJoin);
//        myAmount.setText(myAmt);
      //  myReferCode.setText(myrefer);


}
}
