package com.etm;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by appcraftz on 3/10/17.
 */

public class GiftAdapter extends RecyclerView.Adapter<GiftAdapter.viewHolder> {

    ArrayList<GiftItem> giftItems;
    Context context;

    GiftAdapter(Context context, ArrayList<GiftItem> giftItems){
        this.context = context;
        this.giftItems = giftItems;
    }
    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v1;

        v1 = (LayoutInflater.from(context).inflate(R.layout.giftitem2,null));
        return new viewHolder(v1);
    }

    @Override
    public void onBindViewHolder(viewHolder holder, int position) {

        switch (position){
            case 0:
                holder.giftImage.setImageResource(R.drawable.home_lock);
                break;
            case 1:
                holder.giftImage.setImageResource(R.drawable.solar_charger);
                break;
            case 2:
                holder.giftImage.setImageResource(R.drawable.roti_maker);
                break;
            case 3:
                holder.giftImage.setImageResource(R.drawable.induction);
                break;
            case 4:
                holder.giftImage.setImageResource(R.drawable.sofa);
                break;
            case 5:
                holder.giftImage.setImageResource(R.drawable.gas_saver);
                break;
            case 6:
                holder.giftImage.setImageResource(R.drawable.juicer);
                break;
            case 7:
                holder.giftImage.setImageResource(R.drawable.cookerry7);
                break;
            case 8:
                holder.giftImage.setImageResource(R.drawable.cookery50);
                break;
            case 9:
                holder.giftImage.setImageResource(R.drawable.gas);
                break;
            case 10:
                holder.giftImage.setImageResource(R.drawable.washing_machine);
                break;
            case 11:
                holder.giftImage.setImageResource(R.drawable.inverter);
                break;

            default:
                holder.giftImage.setImageResource(R.mipmap.ic_launcher);
                break;
        }
        holder.joinings.setText(giftItems.get(position).getJoining());
        holder.bankBalance.setText(giftItems.get(position).getBankBalance());
        holder.benefit.setText(giftItems.get(position).getBenefit());
    }

    @Override
    public int getItemCount() {
        return giftItems.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder{

        ImageView giftImage;
        TextView joinings,bankBalance,benefit;
        public viewHolder(View itemView) {
            super(itemView);


            giftImage = (ImageView)itemView.findViewById(R.id.image2);
            joinings = (TextView)itemView.findViewById(R.id.joining);
            bankBalance = (TextView)itemView.findViewById(R.id.bal);
            benefit = (TextView)itemView.findViewById(R.id.gift);
        }
    }
}
