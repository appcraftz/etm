package com.etm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;



import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.security.Key;
import java.util.ArrayList;
import java.util.Calendar;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.R.id.message;

/**
 * Created by Parth on 10/7/2016.
 */

public class ServerAgent {

    private OkHttpClient httpClient;
    private Context con;


    public ServerAgent(Context con)
    {
        httpClient = new OkHttpClient();
        this.con = con;

    }

    //TODO: add server methods here
    public String sendToServer(ArrayList<KeyValueMap> list, String serviceType)
    {
      // String response="";


        try
        {
            RequestBody formBody;
            FormBody.Builder builder= new FormBody.Builder();
//            builder.add("api_key", GlobalVariables.API_KEY);

            for(KeyValueMap value:list)
            {
                builder.add(value.getKey(),value
                .getValue());

                Log.e("KEY--",value.getKey());
                Log.e("VALUE--",value.getValue());

            }
            formBody = builder.build();


            Request request = new Request.Builder()
                    .url(serviceType)
                    .post(formBody)
                    .build();


        Response response = httpClient.newCall(request).execute();
            String resp = response.body().string();

        Log.e("RESP",resp);

        return resp;

        }catch (Exception ex){
            ex.printStackTrace();
            if(ex.getClass() == SocketTimeoutException.class)
            {
                return "CONNECTION_ERROR";
            }
        }
        return "fail";



       // return response;
    }

public String registerUser(SignUpItem signUpItem){

    String resp;
    ArrayList<KeyValueMap> map = new ArrayList<KeyValueMap>();

    map.add(new KeyValueMap("name",signUpItem.getsName()));
    map.add(new KeyValueMap("address",signUpItem.getsAddress()));
    map.add(new KeyValueMap("number",signUpItem.getsMobileNo()));
    map.add(new KeyValueMap("acc_no",signUpItem.getbAccountNo()));
    map.add(new KeyValueMap("bank_address",signUpItem.getbAddress()));
    map.add(new KeyValueMap("IFSC_code",signUpItem.getbIFSC()));
    map.add(new KeyValueMap("adhar_number",signUpItem.getsAadhar()));
    map.add(new KeyValueMap("pan_card",signUpItem.getsPANcard()));
//    String encrypted_password = MD5Hash.md5(signUpItem.getsPassword());
//    Log.e("Encrypted Password",encrypted_password);
    map.add(new KeyValueMap("password",signUpItem.getsPassword()));
    map.add(new KeyValueMap("gender",signUpItem.getsGender()));
    map.add(new KeyValueMap("dist_agency",signUpItem.getaDistAgency()));
    map.add(new KeyValueMap("tal_agency",signUpItem.getaTalAgency()));

    resp = sendToServer(map,Url.register);

    //Log.e("In ServerAgent",resp);
    return resp;
}


    void old()
    {

  /*  public String addOrganization(final String orgName, final String sendTo, final String number, final boolean isOwned) {
        final String orgId =orgName;

        *//*

         $orgName = $_REQUEST['orgName'];
 $ownerName = $_REQUEST['ownerName'];
 $ownerNumber = $_REQUEST['ownerNumber'];
         *//*

        final ArrayList<KeyValueMap> keyValueMapArrayList = new ArrayList<KeyValueMap>();


        if(isOwned) {
            keyValueMapArrayList.add(new KeyValueMap("ownername", orgName));
            keyValueMapArrayList.add(new KeyValueMap("ownernumber", number));

            Log.e("NUMBER",number);
            Log.e("orgname",orgName);
        }else
        {
            keyValueMapArrayList.add(new KeyValueMap("org_id",orgId));
            keyValueMapArrayList.add(new KeyValueMap("number", number));
            keyValueMapArrayList.add(new KeyValueMap("owner_num",sendTo));

        }






                try {
                    String resp;//
                    // = sendToServer(keyValueMapArrayList,Url.addOrganization);
                    if(isOwned)
                        resp = sendToServer(keyValueMapArrayList,Url.addOrganization);
                    else
                        resp = sendToServer(keyValueMapArrayList,Url.addOrganizationRequest);
                    Log.e("JSON RESP",resp);

                    JSONObject obj = new JSONObject(resp);

                 //   JSONObject obj = obj_root.getJSONObject("data");
                    ServerFlags flag = Enum.valueOf(ServerFlags.class,obj.getString("flag"));
                    String Id = "";
                    String empOrgName="";
                    if(flag==ServerFlags.LICENSE_ERROR)
                        return ServerFlags.LICENSE_ERROR.name();

                        if (isOwned) {
                            Id = obj.getString("new_org_id");
                        } else {
                            Id = orgId;

                            empOrgName = obj.getString("org_name");
                        }
                        //  ToDoLists.organizations.add(new OrganizationItem(id,orgName));
                        OrganizationItem item;
                        if (isOwned)
                            item = new OrganizationItem(Id, orgName, 1);
                        else
                            item = new OrganizationItem(Id, empOrgName, 0);

                        item.addToDatabase(con);

                    SqLiteDatabase sqLiteDatabase =  new SqLiteDatabase(con);
                    ToDoLists.organizations = sqLiteDatabase.getAllOrg();
                        //ToDoLists.organizations.add(item);
                        Intent pushNotification = new Intent("organizationAdded");
                        pushNotification.putExtra("message", message);
                        LocalBroadcastManager.getInstance(con).sendBroadcast(pushNotification);

                        return Id;






                } catch (JSONException e) {
                    e.printStackTrace();

                    return "CONNECTION_ERROR";
                }




    }

    public void register(String name, String number, String dob)
    {

        boolean verified = false;
        ArrayList<KeyValueMap> values = new ArrayList<KeyValueMap>();
        values.add(new KeyValueMap("mobile_number",number));

        values.add(new KeyValueMap("name",name));
        values.add(new KeyValueMap("device_token", SPHelper.getSP(con,"regId")));
        values.add(new KeyValueMap("dob",dob));

        Log.e("num",number.replace("+",""));
        Log.e("name",name);
        Log.e("device_token",SPHelper.getSP(con,"regId"));

        try {
            JSONObject object = new JSONObject(sendToServer(values, Url.register));

            String serverprefix = object.getString("server_prefix");



            SPHelper.setSP(con,"server_prefix",serverprefix);
            SPHelper.setSP(con,"name",name);
            SPHelper.setSP(con,"number",number);


        } catch (JSONException e) {
            e.printStackTrace();

        }





    }

    public boolean activate(String code, String number)
    {
        ArrayList<KeyValueMap> values = new ArrayList<KeyValueMap>();
        values.add(new KeyValueMap("num",number));

        values.add(new KeyValueMap("key",code));


        try {
            JSONObject object = new JSONObject(sendToServer(values, Url.activate));

            ServerFlags flag = Enum.valueOf(ServerFlags.class,object.getString("flag"));


            switch (flag)
            {
                case INVALID_CODE:
                    return false;
                case ALREADY_ACTIVATED:
                    return true;

                case ACTIVATION_COMPLETE:
                    return true;

            }




        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    public void approveEmployee(String orgId, String empNo, int flag)
    {
        ArrayList<KeyValueMap> values = new ArrayList<KeyValueMap>();
        values.add(new KeyValueMap("org_id",orgId));

        values.add(new KeyValueMap("emp_number",empNo));
        values.add(new KeyValueMap("flag", String.valueOf(flag)));


        try {
            JSONObject object = new JSONObject(sendToServer(values, Url.acceptEmployee));




        } catch (JSONException e) {
            e.printStackTrace();

        }

    }
    public void trackEmployee(String ownerNo, String empNo, ServerFlags serverFlag, String latLon)
    {
        ArrayList<KeyValueMap> values = new ArrayList<KeyValueMap>();
        values.add(new KeyValueMap("owner_no",ownerNo));

        values.add(new KeyValueMap("emp_no",empNo));
        values.add(new KeyValueMap("track",serverFlag.name()));
        values.add(new KeyValueMap("link",latLon));


        try {
           sendToServer(values, Url.trackLocation);




        } catch (Exception e) {
            e.printStackTrace();

        }

    }
    public boolean verifyOTP(String otp, String number)
    {

        boolean verified = false;
        //TODO: get json object "data" from server

        ArrayList<KeyValueMap> keyValueMapArrayList = new ArrayList<KeyValueMap>();
        keyValueMapArrayList.add(new KeyValueMap("otp",otp));
        keyValueMapArrayList.add(new KeyValueMap("number",number));

        try {
            JSONObject object = new JSONObject(sendToServer(keyValueMapArrayList, Url.verifyOTP));
            String resp = object.getString("verified");
            String serverprefix = object.getString("server_prefix");


            verified = Boolean.parseBoolean(resp);
            SPHelper.setSP(con,"server_prefix",serverprefix);


        } catch (JSONException e) {
            e.printStackTrace();
        }


            return verified;
    }

    public void deligateToDo(String from, String to, String orgCode, ToDoItem item)
    {
        ArrayList<KeyValueMap> keyValueMapArrayList = new ArrayList<KeyValueMap>();
        keyValueMapArrayList.add(new KeyValueMap("assigned_by",from));
        keyValueMapArrayList.add(new KeyValueMap("assigned_to",to));
        keyValueMapArrayList.add(new KeyValueMap("organization_id",orgCode));
        keyValueMapArrayList.add(new KeyValueMap("task_desc",item.getName()));
        keyValueMapArrayList.add(new KeyValueMap("task_id",item.getId()));
        keyValueMapArrayList.add(new KeyValueMap("date",item.getDate()));
        keyValueMapArrayList.add(new KeyValueMap("time",item.getTime()));

        sendToServer(keyValueMapArrayList,Url.deligateToDo);

    }

    public void deleteToDo(ToDoItem toDoItem,OrganizationItem organizationItem)
    {
        ArrayList<KeyValueMap> keyValueMapArrayList = new ArrayList<KeyValueMap>();
        keyValueMapArrayList.add(new KeyValueMap("task_id",toDoItem.getId()));
        keyValueMapArrayList.add(new KeyValueMap("org_id",organizationItem.getId()));
        keyValueMapArrayList.add(new KeyValueMap("dest_no",toDoItem.getDelegateNo()));

        Log.e("DELEGATE NO",toDoItem.getDelegateNo());
        sendToServer(keyValueMapArrayList,Url.deleteTask);
    }

    public void modifyToDo(ToDoItem toDoItem,OrganizationItem organizationItem)
    {
        String priority = toDoItem.getPriority().name();
        Log.e("PRIORITY",priority);
        Log.e("ORG ID",organizationItem.getId());
        String date = toDoItem.getDate();
        String time = toDoItem.getTime();
        String note = toDoItem.getNote();

        ArrayList<KeyValueMap> keyValueMapArrayList = new ArrayList<KeyValueMap>();
        keyValueMapArrayList.add(new KeyValueMap("priority",priority));
        keyValueMapArrayList.add(new KeyValueMap("date",date));
        keyValueMapArrayList.add(new KeyValueMap("time",time));
        keyValueMapArrayList.add(new KeyValueMap("note",note));
        keyValueMapArrayList.add(new KeyValueMap("org_id",organizationItem.getId()));

        keyValueMapArrayList.add(new KeyValueMap("task_id",toDoItem.getId()));

        if(organizationItem.isOwned())
            keyValueMapArrayList.add(new KeyValueMap("edit_by","owner"));
        else
            keyValueMapArrayList.add(new KeyValueMap("edit_by","employee"));



        sendToServer(keyValueMapArrayList,Url.updateTask);



    }

    public void  markToDoCompleted(ToDoItem toDoItem,OrganizationItem orgItem,String latlon)
    {
        ArrayList<KeyValueMap> keyValueMapArrayList = new ArrayList<KeyValueMap>();
        keyValueMapArrayList.add(new KeyValueMap("task_id",toDoItem.getId()));
        keyValueMapArrayList.add(new KeyValueMap("org_id",orgItem.getId()));
        keyValueMapArrayList.add(new KeyValueMap("emp_no",SPHelper.getSP(con,"number")));
        keyValueMapArrayList.add(new KeyValueMap("geo_link",latlon));
        keyValueMapArrayList.add(new KeyValueMap("iscompleated",ServerFlags.ToDo_COMPLETE.name()));


        sendToServer(keyValueMapArrayList,Url.iscomplete);
    }


    public void updateFireBaseToken(String token)
    {
        String number = SPHelper.getSP(con,"number");

        final ArrayList<KeyValueMap> map = new ArrayList<>();
        map.add(new KeyValueMap("number",number));
        map.add(new KeyValueMap("token",token));


     //   task.execute(null,null,null);
        new Thread(new Runnable() {
            @Override
            public void run() {
                sendToServer(map,Url.firebaseTokenUpdate);
            }
        }).start();



    }

    public void applyEmployeeExtension(String key, String orgId)
    {
        String number = SPHelper.getSP(con,"number");

        final ArrayList<KeyValueMap> map = new ArrayList<>();
        map.add(new KeyValueMap("number",number));
        map.add(new KeyValueMap("key",key));
        map.add(new KeyValueMap("org_id",orgId));



                sendToServer(map,Url.empExtension);




    }

    public void applyOrgExtension(String key)
    {
        String number = SPHelper.getSP(con,"number");

        final ArrayList<KeyValueMap> map = new ArrayList<>();
        map.add(new KeyValueMap("number",number));
        map.add(new KeyValueMap("key",key));



        Log.e("Applying key",key);
        sendToServer(map,Url.orgExtension);





    }

    public void sendChatToEmployee(ChatItem chatItem,Context con)
    {
        Calendar calendar = Calendar.getInstance();
        String hour = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
        String minute = String.valueOf(calendar.get(Calendar.MINUTE));

        String time = hour + ":" + minute;
        ArrayList<KeyValueMap> keyValueMapArrayList = new ArrayList<KeyValueMap>();
        keyValueMapArrayList.add(new KeyValueMap("org_id",chatItem.getOrgId()));
        keyValueMapArrayList.add(new KeyValueMap("dest",chatItem.getEmpno()));
        keyValueMapArrayList.add(new KeyValueMap("source_no",SPHelper.getSP(con,"number")));
        keyValueMapArrayList.add(new KeyValueMap("message",chatItem.getMsg()));
        keyValueMapArrayList.add(new KeyValueMap("time",time));
        keyValueMapArrayList.add(new KeyValueMap("flag",chatItem.getFlag().name()));
        keyValueMapArrayList.add(new KeyValueMap("chat_type", ChatTypes.TEXT.name()));




        sendToServer(keyValueMapArrayList,Url.chats);
    }

    public void markAttendance(String number, String org_id)
    {
        Calendar calendar = Calendar.getInstance();
        String hour = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
        String minute = String.valueOf(calendar.get(Calendar.MINUTE));

        String time = hour + ":" + minute;

        ArrayList<KeyValueMap> keyValueMapArrayList = new ArrayList<KeyValueMap>();
        keyValueMapArrayList.add(new KeyValueMap("org_id",org_id));
        keyValueMapArrayList.add(new KeyValueMap("emp_no",number));
        keyValueMapArrayList.add(new KeyValueMap("time",time));
        keyValueMapArrayList.add(new KeyValueMap("flag",ServerFlags.ATTANDANCE_MARKED.name()));
        sendToServer(keyValueMapArrayList,Url.attendance);
    }


    public void enableAttendance(String org_id, String lat, String lon, String startTime)
    {
        Calendar calendar = Calendar.getInstance();
        String hour = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
        String minute = String.valueOf(calendar.get(Calendar.MINUTE));

        String time = hour + ":" + minute;

        ArrayList<KeyValueMap> keyValueMapArrayList = new ArrayList<KeyValueMap>();
        keyValueMapArrayList.add(new KeyValueMap("org_id",org_id));
        keyValueMapArrayList.add(new KeyValueMap("longitude",lon));
        keyValueMapArrayList.add(new KeyValueMap("latitude",lat));
        keyValueMapArrayList.add(new KeyValueMap("start_time",startTime));
        keyValueMapArrayList.add(new KeyValueMap("flag",ServerFlags.AUTO_ATT_ENABLE.name()));
        sendToServer(keyValueMapArrayList,Url.enable_auto_attendance);
    }
    public void disableAttendance(String org_id)
    {
        Calendar calendar = Calendar.getInstance();
        String hour = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
        String minute = String.valueOf(calendar.get(Calendar.MINUTE));

        String time = hour + ":" + minute;

        ArrayList<KeyValueMap> keyValueMapArrayList = new ArrayList<KeyValueMap>();
        keyValueMapArrayList.add(new KeyValueMap("org_id",org_id));

        keyValueMapArrayList.add(new KeyValueMap("flag",ServerFlags.AUTO_ATT_DISABLE.name()));
        sendToServer(keyValueMapArrayList,Url.enable_auto_attendance);
    }
    public void deleteEmployee(String org_id, String empno)
    {

        ArrayList<KeyValueMap> keyValueMapArrayList = new ArrayList<KeyValueMap>();
        keyValueMapArrayList.add(new KeyValueMap("org_id",org_id));

        keyValueMapArrayList.add(new KeyValueMap("emp_no",empno));
        sendToServer(keyValueMapArrayList,Url.deleteEmployee);
    }

    public void setManager(String org_id, String empno)
    {

        ArrayList<KeyValueMap> keyValueMapArrayList = new ArrayList<KeyValueMap>();

        keyValueMapArrayList.add(new KeyValueMap("org_id",org_id));

        keyValueMapArrayList.add(new KeyValueMap("emp_no",empno));

        keyValueMapArrayList.add(new KeyValueMap("flag",ServerFlags.SET_MANAGER.name()));

        sendToServer(keyValueMapArrayList,Url.setManager);
    }

    public String getAttendance(String startDate, String empNo, String orgId)
    {

        ArrayList<KeyValueMap> keyValueMapArrayList = new ArrayList<KeyValueMap>();
        keyValueMapArrayList.add(new KeyValueMap("start_date",startDate));

        keyValueMapArrayList.add(new KeyValueMap("emp_no",empNo));

        keyValueMapArrayList.add(new KeyValueMap("org_id",orgId));
       String resp =  sendToServer(keyValueMapArrayList,Url.get_attendance);

        try {
            JSONObject object = new JSONObject(resp);
            return object.getString("data");
        } catch (JSONException e) {
            e.printStackTrace();
            return "0";
        }

    }
    public String getEfficiency(String startDate, String empNo, String orgId)
    {

        ArrayList<KeyValueMap> keyValueMapArrayList = new ArrayList<KeyValueMap>();
        keyValueMapArrayList.add(new KeyValueMap("start_date",startDate));

        keyValueMapArrayList.add(new KeyValueMap("emp_no",empNo));

        keyValueMapArrayList.add(new KeyValueMap("org_id",orgId));
        String resp =  sendToServer(keyValueMapArrayList,Url.employee_efficiency);

        try {
            JSONObject object = new JSONObject(resp);
            return object.getString("data");
        } catch (JSONException e) {
            e.printStackTrace();
            return "0";
        }

    }
    public void updateLastLocation(Location location){

        ArrayList<KeyValueMap> keyValueMapArrayList = new ArrayList<KeyValueMap>();
        keyValueMapArrayList.add(new KeyValueMap("number",SPHelper.getSP(con,"number")));

      keyValueMapArrayList.add(new KeyValueMap("location", String.valueOf(location.getLatitude())+"," + String.valueOf(location.getLongitude())));
        sendToServer(keyValueMapArrayList,Url.last_location);
    }


    public String addGroup(GroupItem groupItem, Context con) {

        ArrayList<KeyValueMap> keyValueMapArrayList = new ArrayList<KeyValueMap>();

        int totalM = Integer.parseInt(groupItem.getGroupMembers()) + 1;
        keyValueMapArrayList.add(new KeyValueMap("org_id",groupItem.getOrgId()));
        keyValueMapArrayList.add(new KeyValueMap("group_id",groupItem.getGroupId()));
        keyValueMapArrayList.add(new KeyValueMap("group_name",groupItem.getGroupName()));
        keyValueMapArrayList.add(new KeyValueMap("total_members", String.valueOf(totalM)));
        keyValueMapArrayList.add(new KeyValueMap("group_owner",SPHelper.getSP(con,"number")));


        Log.e("Creating Group SA","agent");
        Log.e("Organization ID: ",groupItem.getOrgId());
        Log.e("Group ID: ",groupItem.getGroupId());
        Log.e("GroupName : ",groupItem.getGroupName());
        Log.e("Total Members: ", String.valueOf(totalM));
        Log.e("Created By ",SPHelper.getSP(con,"number"));


        String resp = sendToServer(keyValueMapArrayList,Url.createGroupChat);
        return resp;
    }

    public String addGroupMembers(String groupId, ArrayList<Employee> sample, Context context) {

        ArrayList<KeyValueMap> keyValueMapArrayList = new ArrayList<KeyValueMap>();

        keyValueMapArrayList.add(new KeyValueMap("group_id",groupId));

        Log.e("Group Id",groupId);


        JSONArray employee_name = new JSONArray();
        JSONArray employee_number = new JSONArray();

       employee_name.put(SPHelper.getSP(context,"name"));
        employee_number.put(SPHelper.getSP(context,"number"));
        Log.e("Employee Name in mem: ",SPHelper.getSP(context,"name"));
        Log.e("Employee Number in mem",SPHelper.getSP(context,"number"));

        for (Employee item:sample){



            employee_name.put(item.getName());
            employee_number.put(item.getNumber());

            *//*Log.e("Employee Name in mem: ",item.getName());
            Log.e("Employee Number in mem",item.getNumber());*//*
        }


        String json = employee_name.toString();
        String json2 = employee_number.toString();

        keyValueMapArrayList.add(new KeyValueMap("employee_name",json));
        keyValueMapArrayList.add(new KeyValueMap("employee_number",json2));


        String resp =sendToServer(keyValueMapArrayList,Url.addGroupMembers);

        return resp;
    }


    // Created on 05-Sep-2017
    public void sendGroupChatToEmp(ChatItem chatItem, Activity activity) {
        ArrayList<KeyValueMap> keyValueMapArrayList = new ArrayList<KeyValueMap>();
        Log.e("In ServerAgent","Sending GroupChat");
        keyValueMapArrayList.add(new KeyValueMap("grp_id",chatItem.getOrgId()));
        keyValueMapArrayList.add(new KeyValueMap("own_num",chatItem.getEmpno()));
        keyValueMapArrayList.add(new KeyValueMap("message",chatItem.getMsg()));
        keyValueMapArrayList.add(new KeyValueMap("time",chatItem.getTime()));
        keyValueMapArrayList.add(new KeyValueMap("flag",chatItem.getFlag().name()));

        sendToServer(keyValueMapArrayList,Url.groupChats);

    }

//Both added on 06/09/17
    public ArrayList<NoteItem> getAllNotices(String id) {

        ArrayList<NoteItem> noticeItems = new ArrayList<NoteItem>();

        ArrayList<KeyValueMap> map = new ArrayList<KeyValueMap>();
        map.add(new KeyValueMap("org_id",id));

        Log.e("In SQlite","GetNotices");
        Log.e("Org_id", id);

        try {
            JSONObject object = new JSONObject(sendToServer(map,Url.getNotices));

            JSONArray jsonArray = object.getJSONArray("data");

            if (jsonArray!=null){
                for (int i = 0;i<jsonArray.length();i++) {
                    JSONObject object1 = jsonArray.getJSONObject(i);
                    NoteItem noteItem= new NoteItem(object1.getString("tittle"), object1.getString("message"),object1.getString("message_id"));

                   *//* Log.e("GetNotices","inServerAgent");
                    Log.e("tittle",object1.getString("tittle"));
                    Log.e("message",object1.getString("message"));
                    Log.e("message_id",object1.getString("message_id"));*//*

                    noticeItems.add(noteItem);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("Size of list",""+noticeItems.size());

        return noticeItems;
    }

    public void addNotice(String id, String name, String body) {

        ArrayList<KeyValueMap> keyValueMapArrayList = new ArrayList<KeyValueMap>();
        keyValueMapArrayList.add(new KeyValueMap("org_id",id));
        keyValueMapArrayList.add(new KeyValueMap("tittle",name));
        keyValueMapArrayList.add(new KeyValueMap("message",body));

        sendToServer(keyValueMapArrayList,Url.addNotice);
        Log.e("Adding Notices","ServerAgent");
        Log.e("org_id ",id);
        Log.e("title",name);
        Log.e("Message",body);


    }


    // Added on 06-09-2017
    public ArrayList<ManagerItem> getManagers(String org_id) {

        ArrayList<ManagerItem> managers = new ArrayList<ManagerItem>();

        ArrayList<KeyValueMap> map = new ArrayList<KeyValueMap>();

        map.add(new KeyValueMap("org_id",org_id));
        //Log.e("In ServerAgent","get_managers");
//        Log.e("org_id",org_id);

        try {
            JSONObject object = new JSONObject(sendToServer(map,Url.fetchManagers));

            JSONArray jsonArray = object.getJSONArray("data");

            if (jsonArray!=null){
                for (int i = 0;i<jsonArray.length();i++){
                    JSONObject object1 = jsonArray.getJSONObject(i);
                    ManagerItem managerItem = new ManagerItem(object1.getString("manager_name"),object1.getString("manager_number"));
          *//*          Log.e("Managers","inServerAgent");
                    Log.e("Name",object1.getString("manager_name"));
                    Log.e("Number",object1.getString("manager_number"));
          *//*          managers.add(managerItem);
                }

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        *//*managers.add(new ManagerItem("Parth","8149263265"));
        managers.add(new ManagerItem("Sandeep","9766789876"));
        *//*return managers;
    }

    public void deleteNotice(String org_id, String noticeId) {

        ArrayList<KeyValueMap> map = new ArrayList<KeyValueMap>();

        map.add(new KeyValueMap("org_id",org_id));
        map.add(new KeyValueMap("message_id",noticeId));

        Log.e("DeletingNotice","In ServerAgent");
        Log.e("org_id",org_id);
        Log.e("notice_id",noticeId);

        sendToServer(map,Url.deleteNotice);
    }

    public void deleteGroupMember(String groupId, String number) {

        ArrayList<KeyValueMap> map = new ArrayList<KeyValueMap>();

        map.add(new KeyValueMap("group_id",groupId));
        map.add(new KeyValueMap("emp_num",number));

        Log.e("DeletingMember","In ServerAgent");
        Log.e("group_id",groupId);
        Log.e("emp_num",number);

        sendToServer(map,Url.deleteGroupMember);
    }*/
    }

    public ArrayList<KeyValueMap> login(String user, String pwd) {

        ArrayList<KeyValueMap> map = new ArrayList<KeyValueMap>();

      //  String encrypted_password = MD5Hash.md5(pwd);
      //  Log.e("Encrypted Password",encrypted_password);
        map.add(new KeyValueMap("number",user));
        map.add(new KeyValueMap("password",pwd));
//BR7383777

        ArrayList<KeyValueMap> response = new ArrayList<KeyValueMap>();
       // String response =null;
        try {
            JSONObject obj = new JSONObject(sendToServer(map,Url.login));

            if (!obj.getString("data").equals("EMPTY_FIELDS")){
                response.add(new KeyValueMap("FLAG",obj.getString("data")));
                response.add(new KeyValueMap("REFER_CODE",obj.getString("refer_code")));
                response.add(new KeyValueMap("REFERRED_BY",obj.getString("referred_by")));
            }else {
                response.add(new KeyValueMap("FLAG",obj.getString("data")));
                response.add(new KeyValueMap("REFER_CODE",null));
                response.add(new KeyValueMap("REFERRED_BY",null));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return response;
    }

    public String verifyOtp(String number, String otp) {
        String response = null;
        ArrayList<KeyValueMap> map = new ArrayList<KeyValueMap>();

        map.add(new KeyValueMap("number",number));
        map.add(new KeyValueMap("otp",otp));

try {
    JSONObject obj = new JSONObject(sendToServer(map,Url.verifyOtp));

    response = obj.getString("data");
} catch (JSONException e) {
    e.printStackTrace();
}


        return response;


    }

    public String verifyReferralCode(String number, String code) {
        ArrayList<KeyValueMap> map = new ArrayList<KeyValueMap>();

        map.add(new KeyValueMap("number",number));
        map.add(new KeyValueMap("key",code));

        String resp= null;
        try {
            JSONObject object = new JSONObject(sendToServer(map,Url.verifyReferralCode));

            return resp = object.getString("data");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public ArrayList<MaxItem> getMaxUsers() {
       ArrayList<KeyValueMap> map = new ArrayList<KeyValueMap>();
        map.add(new KeyValueMap("maxItems","maxItems"));

        ArrayList<MaxItem> maxItems = new ArrayList<MaxItem>();
        try {
            JSONObject object = new JSONObject(sendToServer(map,Url.getMaxItems));

            JSONArray jsonArray = object.getJSONArray("data");

            for (int i=0; i<jsonArray.length();i++){
                JSONObject obj = jsonArray.getJSONObject(i);

                maxItems.add(new MaxItem(obj.getString("refer_code"),obj.getString("count")));

            }
            return maxItems;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<KeyValueMap> myJoining(String myNumber) {

        ArrayList<KeyValueMap> map = new ArrayList<KeyValueMap>();
        map.add(new KeyValueMap("number",myNumber));

        Log.e("Getting top users","myJoining");
        ArrayList<KeyValueMap> resp = new ArrayList<KeyValueMap>();
        try {
            JSONObject object = new JSONObject(sendToServer(map,Url.getMyJoining));

                Log.e("Size and ASD",""+object.getString("user_count")+" : "+object.getString("value"));
                resp.add(new KeyValueMap("count",object.getString("user_count")));
                resp.add(new KeyValueMap("amount",object.getString("value")));
            return resp;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public ArrayList<KeyValueMap> getReferredBy(String referred_by) {
        ArrayList<KeyValueMap> maps = new ArrayList<>();
        maps.add(new KeyValueMap("number",referred_by));

        ArrayList<KeyValueMap> resp = new ArrayList<>();
        try {
            JSONObject object = new JSONObject(sendToServer(maps,Url.getReferredByDetails));

            if (object.getString("data").equals("SUCCESS")){
                resp.add(new KeyValueMap("name",object.getString("name")));
                resp.add(new KeyValueMap("code",object.getString("code")));
            }
            else {
                resp.add(new KeyValueMap("name",null));
                resp.add(new KeyValueMap("code",null));
            }

            return resp;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
