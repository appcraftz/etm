package com.etm;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class SignUp extends AppCompatActivity {

    String name,sAddress, sMobileNo, bAccountNo, bAddress, bIFSC, sAadhar, sPANcard,sPassword,gender = null,distAgency = null,tAgency = null;
    EditText rName,rAddress,rMobileNo,rAccountNo,rBankAddress,rIFSCCode,rAadharCardNo,rPANCardNo,rPassword;
    RadioGroup rGender,rDAgency,rTAgency,rPaymentMode;
    CheckBox rAgree;
    Button rSubmit;
    ServerAgent agent;
    RadioButton radioButton;

    ProgressDialog progressDialog;
    NetworkStatus networkStatus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
    }

    @Override
    protected void onStart() {
        super.onStart();
        agent = new ServerAgent(SignUp.this);
        setupViews();
    }

    private void setupViews() {

        networkStatus = NetworkStatus.getInstance(SignUp.this);
        progressDialog = new ProgressDialog(SignUp.this);

        //------EditTexts
        rName = (EditText)findViewById(R.id.rName);
        rAddress = (EditText)findViewById(R.id.rAddress);
        rMobileNo = (EditText)findViewById(R.id.rMobileNo);
        rAccountNo = (EditText)findViewById(R.id.rAccountNo);
        rBankAddress = (EditText)findViewById(R.id.rBankAddress);
        rIFSCCode = (EditText)findViewById(R.id.rIFSCCode);
        rAadharCardNo = (EditText)findViewById(R.id.rAadharCardNo);
        rPANCardNo = (EditText)findViewById(R.id.rPANCardNo);
        rPassword = (EditText)findViewById(R.id.rPassword);


        //------RadioGroups
        rGender = (RadioGroup)findViewById(R.id.rGender);
        rDAgency = (RadioGroup)findViewById(R.id.rDAgency);
        rTAgency = (RadioGroup)findViewById(R.id.rTAgency);
        rPaymentMode = (RadioGroup)findViewById(R.id.rPaymentMode);

        //------ChechBox
        rAgree = (CheckBox)findViewById(R.id.rAgree);

        //------Button
        rSubmit = (Button)findViewById(R.id.rSubmit);



        rAgree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b)
                    rSubmit.setClickable(true);
                else
                    rSubmit.setClickable(false);

            }
        });

        rGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                int selectedid = radioGroup.getCheckedRadioButtonId();
                if (selectedid==R.id.rMale)
                {
                    radioButton = (RadioButton) findViewById(selectedid);
                    gender = radioButton.getText().toString();
                }else if (selectedid == R.id.rFemale){
                    radioButton = (RadioButton) findViewById(selectedid);
                    gender = radioButton.getText().toString();
                }else gender = null;


            }
        });

        rDAgency.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                int selectedid = radioGroup.getCheckedRadioButtonId();
                if (selectedid==R.id.rDname)
                {
                    radioButton = (RadioButton) findViewById(selectedid);
                    distAgency = radioButton.getText().toString();
                }else if (selectedid == R.id.rDSelf){
                    radioButton = (RadioButton) findViewById(selectedid);
                    distAgency = radioButton.getText().toString();
                }else distAgency = null;


            }
        });

        rTAgency.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                int selectedid = radioGroup.getCheckedRadioButtonId();
                if (selectedid==R.id.rTname)
                {
                    radioButton = (RadioButton) findViewById(selectedid);
                    tAgency = radioButton.getText().toString();
                }else if (selectedid == R.id.rTSelf){
                    radioButton = (RadioButton) findViewById(selectedid);
                    tAgency = radioButton.getText().toString();
                }else tAgency = null;


            }
        });
        //Enabling or Disabling Submit Button On I Agree CheckBox Checked
        //gender = ((RadioButton)findViewById(rGender.getCheckedRadioButtonId())).getText().toString();

        rSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (rName.getText().toString().equals("") ||
                        rAddress.getText().toString().equals("") ||
                        rMobileNo.getText().toString().equals("") ||
                        rAccountNo.getText().toString().equals("") ||
                        rBankAddress.getText().toString().equals("") ||
                        rIFSCCode.getText().toString().equals("") ||
                        rAadharCardNo.getText().toString().equals("") ||
                        rPassword.getText().toString().equals("") ||
                        rPANCardNo.getText().toString().equals("") ||
                        gender == null ||
                        distAgency == null ||
                        tAgency == null
                        ){
                    Toast.makeText(SignUp.this, "Empty Fields not Allowed...!", Toast.LENGTH_SHORT).show();
                }else if (rMobileNo.getText().toString().length() < 10){

                    Toast.makeText(SignUp.this, "Please Check your Mobile No...", Toast.LENGTH_SHORT).show();
                }
                else {
                    if (networkStatus.isOnline()) {
                        new AsyncTask<Void, Void, String>() {

                            @Override
                            protected void onPreExecute() {
                                super.onPreExecute();
                                name = rName.getText().toString();
                                sAddress = rAddress.getText().toString();
                                sMobileNo = rMobileNo.getText().toString();
                                bAccountNo = rAccountNo.getText().toString();
                                bAddress = rBankAddress.getText().toString();
                                bIFSC = rIFSCCode.getText().toString();
                                sAadhar = rAadharCardNo.getText().toString();
                                sPANcard = rPANCardNo.getText().toString();
                                sPassword = rPassword.getText().toString();
                                gender = ((RadioButton) findViewById(rGender.getCheckedRadioButtonId())).getText().toString();
                                distAgency = ((RadioButton) findViewById(rDAgency.getCheckedRadioButtonId())).getText().toString();
                                tAgency = ((RadioButton) findViewById(rTAgency.getCheckedRadioButtonId())).getText().toString();

                                progressDialog.setMessage("Creating Account...");
                                progressDialog.setCancelable(false);
                                progressDialog.show();
                            }

                            @Override
                            protected String doInBackground(Void... voids) {

                                String responce = agent.registerUser(new SignUpItem(name, sAddress, sMobileNo, bAccountNo, bAddress, bIFSC, sAadhar, sPANcard, sPassword, gender, distAgency, tAgency));
                                // Log.e("Response in Signup",responce);
                                return responce;
                            }

                            @Override
                            protected void onPostExecute(String s) {
                                super.onPostExecute(s);

                                progressDialog.dismiss();
                                Intent intent = new Intent(SignUp.this, OTP.class);
                                intent.putExtra("number", sMobileNo);
                                startActivity(intent);
                                finish();

                            }
                        }.execute(null, null, null);
                    }else {
                        Toast.makeText(SignUp.this, "Check Your Internet Connection...!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


    }
}
