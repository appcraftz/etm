package com.etm;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ReferralCode extends AppCompatActivity {

    EditText referralCode;
    Button submit,skip;
    ServerAgent agent;
    NetworkStatus networkStatus;
    String number,code;
    TextView tag;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referral_code);

        progressDialog = new ProgressDialog(ReferralCode.this);
        networkStatus = NetworkStatus.getInstance(ReferralCode.this);
        referralCode = (EditText)findViewById(R.id.edReferralCode);
        submit = (Button)findViewById(R.id.btnSaveReferralCode);
        skip = (Button)findViewById(R.id.btnSkipReferral);
        tag = (TextView)findViewById(R.id.tag);


        agent = new ServerAgent(ReferralCode.this);

/*
        if (!(SPHelper.getSP(ReferralCode.this,"referred")).equals("none")){
            referralCode.setEnabled(false);
            if (!(SPHelper.getSP(ReferralCode.this,"code")).equals("none")){
                referralCode.setText(SPHelper.getSP(ReferralCode.this,"code"));
            }
        }*/

        if ((SPHelper.getSP(ReferralCode.this,"referred")).equals("true"))
        {
            skip.setVisibility(View.GONE);
            submit.setVisibility(View.GONE);
            new AsyncTask<Void, Void, ArrayList<KeyValueMap>>() {

                @Override
                protected ArrayList<KeyValueMap> doInBackground(Void... voids) {
                    ArrayList<KeyValueMap> maps = agent.getReferredBy(SPHelper.getSP(ReferralCode.this,"myNumber"));
                    return maps;
                }

                @Override
                protected void onPostExecute(ArrayList<KeyValueMap> keyValueMaps) {
                    super.onPostExecute(keyValueMaps);

                    if (keyValueMaps!=null){
                        if (keyValueMaps.get(0).getValue()!=null){
                            referralCode.setEnabled(false);
                            referralCode.setText(keyValueMaps.get(0).getValue());
                            submit.setVisibility(View.GONE);
                            tag.setText("You are referred by");
                        }else{
                            referralCode.setEnabled(true);
                        }
                    }
                }
            }.execute(null,null,null);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (referralCode.getText().toString().equals("")){
                    Toast.makeText(ReferralCode.this, "Empty Fields not Allowed...!", Toast.LENGTH_SHORT).show();
                }else {
if (networkStatus.isOnline()) {
    new AsyncTask<Void, Void, String>() {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setCancelable(false);
            progressDialog.setMessage("Valicating Referral Code Entered...");
            progressDialog.show();

            number = SPHelper.getSP(ReferralCode.this, "myNumber");
            code = referralCode.getText().toString();
        }

        @Override
        protected String doInBackground(Void... params) {
            String resp = agent.verifyReferralCode(number, code);
            return resp;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            progressDialog.dismiss();
            if (s != null) {
                if (s.equals("inserted")) {
                    SPHelper.setSP(ReferralCode.this, "referred", "true");
                    SPHelper.setSP(ReferralCode.this,"code",code);

                    submit.setVisibility(View.GONE);
                    skip.setVisibility(View.GONE);

                    Toast.makeText(ReferralCode.this, "REFER CODE APPLIED SUCCESSFULLY", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(ReferralCode.this,MainActivity.class));
                    finish();
                    new AsyncTask<Void, Void, ArrayList<KeyValueMap>>() {

                        @Override
                        protected ArrayList<KeyValueMap> doInBackground(Void... voids) {
                            ArrayList<KeyValueMap> maps = agent.getReferredBy(SPHelper.getSP(ReferralCode.this,"myNumber"));
                            return maps;
                        }

                        @Override
                        protected void onPostExecute(ArrayList<KeyValueMap> keyValueMaps) {
                            super.onPostExecute(keyValueMaps);

                            if (keyValueMaps!=null){
                                if (keyValueMaps.get(0).getValue()!=null){
                                    referralCode.setEnabled(false);
                                    referralCode.setText(keyValueMaps.get(0).getValue());
                                    submit.setVisibility(View.GONE);
                                    tag.setText("You are referred by");
                                }else{
                                    referralCode.setEnabled(true);
                                }
                            }
                        }
                    }.execute(null,null,null);

                } else {
                    Toast.makeText(ReferralCode.this, "Invalid Referral Code", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }.execute(null, null, null);
}else {
    Toast.makeText(ReferralCode.this, "Check Your Internet Connection...!", Toast.LENGTH_SHORT).show();
}
                }
            }
        });


        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReferralCode.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
