package com.etm;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

public class OTP extends AppCompatActivity {

    BroadcastReceiver receiver;
    ProgressDialog progressDialog;
    EditText enteredOTP;
    Button verifyOtp;
    ServerAgent agent;
    String otp;
    String number;
    ProgressBar progressbar;
    NetworkStatus networkStatus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        number = getIntent().getExtras().getString("number");
        Log.e("Number In OTP.java",number);


    }

    @Override
    protected void onStart() {
        super.onStart();
networkStatus = NetworkStatus.getInstance(OTP.this);
agent = new ServerAgent(OTP.this);

        enteredOTP = (EditText)findViewById(R.id.enterOTP);
        verifyOtp = (Button)findViewById(R.id.verifyOTP);

        progressbar = (ProgressBar)findViewById(R.id.progressIndicator);

     /*   Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                progressbar.setVisibility(View.VISIBLE);
                finish();
            }
        },5000);*/

        verifyOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (enteredOTP.getText().toString().equals(""))
                    Toast.makeText(OTP.this, "Empty Fields not allowed...", Toast.LENGTH_SHORT).show();
                else{
                    if (networkStatus.isOnline()) {
                        new AsyncTask<Void, Void, String>() {
                            @Override
                            protected void onPreExecute() {
                                super.onPreExecute();
                                otp = enteredOTP.getText().toString();
                                progressbar.setVisibility(View.VISIBLE);
                            }

                            @Override
                            protected String doInBackground(Void... voids) {

                                String response = agent.verifyOtp(number, otp);
                                return response;
                            }

                            @Override
                            protected void onPostExecute(String s) {
                                super.onPostExecute(s);
                                progressbar.setVisibility(View.GONE);
                                if (s.equals("SUCESS")) {
                                    Intent intent = new Intent(OTP.this, Login.class);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Toast.makeText(OTP.this, "Please enter valid OTP...", Toast.LENGTH_SHORT).show();
                                }
                                progressbar.setVisibility(View.GONE);

                            }
                        }.execute(null, null, null);
                    }else {
                        Toast.makeText(OTP.this, "Check Your Internet Connection...!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });



        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equalsIgnoreCase("otp")) {
                    final String message = intent.getStringExtra("message");

                    new AsyncTask<Void, Void, String>() {
                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            enteredOTP.setText(message);
                        }

                        @Override
                        protected String doInBackground(Void... voids) {
                            String response = agent.verifyOtp(number,message);
                            return response;
                        }

                        @Override
                        protected void onPostExecute(String s) {
                            super.onPostExecute(s);
                            if (s.equals("SUCESS")){
                                Intent intent = new Intent(OTP.this,Login.class);
                                startActivity(intent);
                                finish();
                            }else {
                                Toast.makeText(OTP.this, "Please enter valid OTP...", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }.execute(null,null,null);



                }
            }
        };

    }
    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

}
